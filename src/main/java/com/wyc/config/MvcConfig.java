//package com.wyc.config;
//
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.Ordered;
//import org.springframework.web.servlet.config.annotation.*;
//
//
//@Configuration
//@ComponentScan
//@EnableWebMvc
//public class MvcConfig implements WebMvcConfigurer {
//    @Override
//    public void configurePathMatch(PathMatchConfigurer configurer) {
//       configurer.setUseSuffixPatternMatch(false);
//       configurer.setUseRegisteredSuffixPatternMatch(true);
//    }
//
//    @Override
//    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//        configurer.favorPathExtension(false);
//    }
//
//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//
////        registry.addViewController("/").setViewName(".action");
////        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
//        WebMvcConfigurer.super.addViewControllers(registry);
//    }
//}
