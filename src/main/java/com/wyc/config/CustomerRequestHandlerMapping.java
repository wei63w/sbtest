//package com.wyc.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.MergedAnnotation;
//import org.springframework.core.annotation.MergedAnnotations;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.servlet.handler.RequestMatchResult;
//import org.springframework.web.servlet.mvc.condition.ConsumesRequestCondition;
//import org.springframework.web.servlet.mvc.condition.RequestCondition;
//import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
//import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
//
//import javax.servlet.http.HttpServletRequest;
//import java.lang.reflect.Method;
//import java.lang.reflect.Parameter;
//import java.util.Map;
//import java.util.Set;
//import java.util.function.Predicate;
//
//
////@Configuration
//public class CustomerRequestHandlerMapping extends RequestMappingHandlerMapping {
//
//    private RequestMappingInfo.BuilderConfiguration config = new RequestMappingInfo.BuilderConfiguration();
//
//
//    public CustomerRequestHandlerMapping() {
//        super();
//    }
//
//    @Override
//    @SuppressWarnings("deprecation")
//    public void afterPropertiesSet() {
//        this.config = new RequestMappingInfo.BuilderConfiguration();
//        this.config.setUrlPathHelper(getUrlPathHelper());
//        this.config.setPathMatcher(getPathMatcher());
//        this.config.setSuffixPatternMatch(useSuffixPatternMatch());
//        this.config.setTrailingSlashMatch(useTrailingSlashMatch());
//        this.config.setRegisteredSuffixPatternMatch(useRegisteredSuffixPatternMatch());
//        this.config.setContentNegotiationManager(getContentNegotiationManager());
//
//        super.afterPropertiesSet();
//    }
//
//    @Override
//    public void setUseTrailingSlashMatch(boolean useTrailingSlashMatch) {
//        super.setUseTrailingSlashMatch(useTrailingSlashMatch);
//    }
//
//    @Override
//    public void setPathPrefixes(Map<String, Predicate<Class<?>>> prefixes) {
//        super.setPathPrefixes(prefixes);
//    }
//
//    @Override
//    public Map<String, Predicate<Class<?>>> getPathPrefixes() {
//        return super.getPathPrefixes();
//    }
//
//    @Override
//    protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
//        return super.getMappingForMethod(method, handlerType);
//    }
//
//    @Override
//    protected RequestCondition<?> getCustomMethodCondition(Method method) {
//        return super.getCustomMethodCondition(method);
//    }
//
//    @Override
//    public void registerMapping(RequestMappingInfo mapping, Object handler, Method method) {
//        super.registerMapping(mapping, handler, method);
//    }
//
//    @Override
//    protected void registerHandlerMethod(Object handler, Method method, RequestMappingInfo mapping) {
//        super.registerHandlerMethod(handler, method, mapping);
//        updateConsumesCondition(mapping, method);
//    }
//    private void updateConsumesCondition(RequestMappingInfo info, Method method) {
//        ConsumesRequestCondition condition = info.getConsumesCondition();
//        if (!condition.isEmpty()) {
//            for (Parameter parameter : method.getParameters()) {
//                MergedAnnotation<RequestBody> annot = MergedAnnotations.from(parameter).get(RequestBody.class);
//                if (annot.isPresent()) {
//                    condition.setBodyRequired(annot.getBoolean("required"));
//                    break;
//                }
//            }
//        }
//    }
//    @Override
//    public RequestMatchResult match(HttpServletRequest request, String pattern) {
//
//        RequestMappingInfo info = RequestMappingInfo.paths(pattern).options(this.config).build();
//        RequestMappingInfo matchingInfo = info.getMatchingCondition(request);
//        if (matchingInfo == null) {
//            return null;
//        }
//        Set<String> patterns = matchingInfo.getPatternsCondition().getPatterns();
//        String lookupPath = getUrlPathHelper().getLookupPathForRequest(request, LOOKUP_PATH);
//        return new RequestMatchResult(patterns.iterator().next(), lookupPath, getPathMatcher());
//    }
//}
